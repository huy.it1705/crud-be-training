import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import 'dotenv/config';
import { CoffeesModule } from './coffees/coffees.module';
@Module({
  imports: [MongooseModule.forRoot(process.env.MONGODB_URL, {
    useNewUrlParser: true,
  }),
  CoffeesModule,
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

import { Controller, Get, Post, Body, Param, Put, Delete } from '@nestjs/common';
import { CoffeesService } from './coffees.service';
import { CreateCoffees } from './dto/create-coffees';
import { Coffees } from './interfaces/coffees.interface';
import { ApiTags, ApiParam } from '@nestjs/swagger';

@ApiTags('coffees')
@Controller('coffees')
export class CoffeesController {
    constructor(private coffeesService: CoffeesService) { }
    @Get()
    getAllCoffees(): Promise<Coffees[]> {
        return this.coffeesService.getAllCoffees();
    }
    @ApiParam({name: 'id'})
    @Get(':id')
    getCoffeeById(@Param('id') id) {
        return this.coffeesService.getCoffeeById(id);
    }
    @Post()
    createCoffees(@Body() createCoffees: CreateCoffees): Promise<Coffees> {
        return this.coffeesService.createCoffees(createCoffees);
    }
    @ApiParam({name: 'id'})
    @Put(':id')
    updateCoffee(@Param('id') id, @Body() updateCoffee: CreateCoffees): Promise<Coffees> {
        return this.coffeesService.updateCoffee(id, updateCoffee);
    }
    @ApiParam({name: 'id'})
    @Delete(':id')
    removeCoffee(@Param('id') id): Promise<Coffees> {
        return this.coffeesService.removeCoffee(id);
    }
}

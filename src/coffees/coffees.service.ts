import { Injectable } from '@nestjs/common';
import { Coffees } from './interfaces/coffees.interface';
import { CreateCoffees } from './dto/create-coffees';
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose';
@Injectable()
export class CoffeesService {
    constructor(@InjectModel('Coffees') private readonly coffesModel: Model<Coffees>) {}
    async getAllCoffees(): Promise<Coffees[]> {
        return await this.coffesModel.find().exec();
    }
    async getCoffeeById(id: string): Promise<Coffees> {
        return await this.coffesModel.findById(id).exec();
    } 
    async createCoffees(coffees: Coffees): Promise<Coffees> {
        const newCoffees = await new this.coffesModel(coffees);
        return newCoffees.save();
    }
    async updateCoffee(id: string, updateCoffe: CreateCoffees): Promise<Coffees> {
        return await this.coffesModel.findByIdAndUpdate(id, updateCoffe, {new: true});
    }
    async removeCoffee(id: string): Promise<Coffees> {
        return await this.coffesModel.findByIdAndRemove(id);
    }
}

// import {  } from '@nestjs/swagger';

import { ApiProperty } from "@nestjs/swagger";

export class CreateCoffees {
    @ApiProperty()
    readonly title: string;
    
    @ApiProperty()
    readonly author: string;
}
import * as mongoose from 'mongoose';

export const CoffeeSchema = new mongoose.Schema({
    title: String,
    author: String
});
import { Module } from '@nestjs/common';
import { CoffeesService } from './coffees.service';
import { CoffeesController } from './coffees.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CoffeeSchema } from './schemas/coffees.schema';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Coffees', schema: CoffeeSchema}])],
    controllers: [CoffeesController],
    providers: [CoffeesService],
})
export class CoffeesModule {

}
